package com.circlubikz;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.circlubikz.model.ItemBik;
import com.circlubikz.model.ModelGroupsHolder;

public class MainActivity extends Activity {

	MainView mMainView;
	ModelGroupsHolder mModelGroupsHolder;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getActionBar().hide();
		new CirclubikApplication(this);
		mMainView = new MainView(this);		
		
		mModelGroupsHolder = new ModelGroupsHolder(this);
		mMainView.setModel(mModelGroupsHolder);
//		LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_main, null);
//		layout.addView(mMainView);
		setContentView(mMainView);
	}

}
