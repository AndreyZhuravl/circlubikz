package com.circlubikz.model.modelfabrics;

import java.util.ArrayList;

import android.graphics.Color;
import android.graphics.Point;

import com.circlubikz.model.AbsModel;
import com.circlubikz.model.ItemBik;
import com.circlubikz.model.ModelGroup;

public class SevenGroupsModel extends AbsModel {
	float radius = 50;
	String color1 = "#e11212";
	String color2 = "#fcfe00";
	String color3 = "#26c215";
	String color4 = "#08beec";
	String color5 = "#0037b4";
	String color6 = "#6a1c8a";

	Point pointCenterGroup1;
	Point point0;
	Point point1;
	Point point2;
	Point point3;
	Point point4;
	Point point5;

	Point pointCenterGroup2;
	Point point6;
	Point point7;
	Point point8;
	Point point9;

	Point pointCenterGroup3;
	Point point10;
	Point point11;
	Point point12;

	Point pointCenterGroupCenter;

	Point pointCenterGroup4;
	Point point13;
	Point point14;
	Point point15;

	Point pointCenterGroup5;
	Point point16;
	Point point17;
	Point point18;

	Point pointCenterGroup6;
	Point point19;
	Point point20;
	Point point21;

	Point pointCenterGroup7;
	Point point22;
	Point point23;

	public SevenGroupsModel() {
		super();
		initPoints();
		initGroupsHolder();
	}

	Point shiftAndScale(float x, float y) {
		float xScale = (float) 0.8;
		float yScale = (float) 0.8;
		float xShift = (float) 50;
		float yShift = (float) 140;
		return new Point((int) (x * xScale + xShift), (int) (y * yScale + yShift));
	}

	private void initPoints() {

		pointCenterGroup1 = shiftAndScale(268, 200);
		point0 = shiftAndScale(144, 268);
		point1 = shiftAndScale(144, 128);
		point2 = shiftAndScale(270, 54);
		point3 = shiftAndScale(396, 128);
		point4 = shiftAndScale(396, 268);
		point5 = shiftAndScale(270, 340);

		pointCenterGroup2 = shiftAndScale(522, 200);
		point6 = shiftAndScale(522, 54);
		point7 = shiftAndScale(648, 128);
		point8 = shiftAndScale(648, 268);
		point9 = shiftAndScale(522, 340);

		pointCenterGroup3 = shiftAndScale(396, 412);
		point10 = shiftAndScale(522, 480);
		point11 = shiftAndScale(396, 552);
		point12 = shiftAndScale(268, 482);

		pointCenterGroup4 = shiftAndScale(144, 412);
		point13 = shiftAndScale(144, 556);
		point14 = shiftAndScale(18, 482);
		point15 = shiftAndScale(18, 342);

		pointCenterGroup5 = shiftAndScale(640, 412);
		point16 = shiftAndScale(766, 342);
		point17 = shiftAndScale(766, 482);
		point18 = shiftAndScale(640, 556);

		pointCenterGroup6 = shiftAndScale(522, 626);
		point19 = shiftAndScale(640, 696);
		point20 = shiftAndScale(522, 770);
		point21 = shiftAndScale(396, 696);

		pointCenterGroup7 = shiftAndScale(270, 626);
		point22 = shiftAndScale(270, 770);
		point23 = shiftAndScale(144, 696);

		pointCenterGroupCenter = shiftAndScale(396, 412);

	}

	private void addPointsToArray() {
		mItemBikList.add(new ItemBik(point0, radius, Color.parseColor(color2)));
		mItemBikList.add(new ItemBik(point1, radius, Color.parseColor(color1)));
		mItemBikList.add(new ItemBik(point2, radius, Color.parseColor(color1)));
		mItemBikList.add(new ItemBik(point3, radius, Color.parseColor(color1)));
		mItemBikList.add(new ItemBik(point4, radius, Color.parseColor(color2)));
		mItemBikList.add(new ItemBik(point5, radius, Color.parseColor(color3)));

		mItemBikList.add(new ItemBik(point6, radius, Color.parseColor(color1)));
		mItemBikList.add(new ItemBik(point7, radius, Color.parseColor(color1)));
		mItemBikList.add(new ItemBik(point8, radius, Color.parseColor(color2)));
		mItemBikList.add(new ItemBik(point9, radius, Color.parseColor(color3)));

		mItemBikList.add(new ItemBik(point10, radius, Color.parseColor(color4)));
		mItemBikList.add(new ItemBik(point11, radius, Color.parseColor(color5)));
		mItemBikList.add(new ItemBik(point12, radius, Color.parseColor(color4)));

		mItemBikList.add(new ItemBik(point13, radius, Color.parseColor(color5)));
		mItemBikList.add(new ItemBik(point14, radius, Color.parseColor(color4)));
		mItemBikList.add(new ItemBik(point15, radius, Color.parseColor(color3)));

		mItemBikList.add(new ItemBik(point16, radius, Color.parseColor(color3)));
		mItemBikList.add(new ItemBik(point17, radius, Color.parseColor(color4)));
		mItemBikList.add(new ItemBik(point18, radius, Color.parseColor(color5)));

		mItemBikList.add(new ItemBik(point19, radius, Color.parseColor(color6)));
		mItemBikList.add(new ItemBik(point20, radius, Color.parseColor(color6)));
		mItemBikList.add(new ItemBik(point21, radius, Color.parseColor(color6)));

		mItemBikList.add(new ItemBik(point22, radius, Color.parseColor(color6)));
		mItemBikList.add(new ItemBik(point23, radius, Color.parseColor(color6)));
	}

	private void initGroupsHolder() {
		addPointsToArray();
		mHolder.add(createModelGroup3());
		mHolder.add(createModelGroup1());
		mHolder.add(createModelGroup2());
		mHolder.add(createModelGroup4());
		mHolder.add(createModelGroup5());
		mHolder.add(createModelGroup6());
		mHolder.add(createModelGroup7());
	}

	public ModelGroup createModelGroup1() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();

		arrayPoint.add(point0);
		arrayPoint.add(point1);
		arrayPoint.add(point2);
		arrayPoint.add(point3);
		arrayPoint.add(point4);
		arrayPoint.add(point5);

		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(0);
		permutation.add(1);
		permutation.add(2);
		permutation.add(3);
		permutation.add(4);
		permutation.add(5);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroup1, arrayPoint);
		return modelGroup;
	}

	public ModelGroup createModelGroup2() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();

		arrayPoint.add(point4);
		arrayPoint.add(point3);
		arrayPoint.add(point6);
		arrayPoint.add(point7);
		arrayPoint.add(point8);
		arrayPoint.add(point9);

		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(4);
		permutation.add(3);
		permutation.add(6);
		permutation.add(7);
		permutation.add(8);
		permutation.add(9);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroup2, arrayPoint);
		return modelGroup;
	}

	public ModelGroup createModelGroup3() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();

		arrayPoint.add(point5);
		arrayPoint.add(point4);
		arrayPoint.add(point9);
		arrayPoint.add(point10);
		arrayPoint.add(point11);
		arrayPoint.add(point12);

		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(5);
		permutation.add(4);
		permutation.add(9);
		permutation.add(10);
		permutation.add(11);
		permutation.add(12);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroup3, arrayPoint);
		return modelGroup;
	}

	public ModelGroup createModelGroup4() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();

		arrayPoint.add(point0);
		arrayPoint.add(point5);
		arrayPoint.add(point12);
		arrayPoint.add(point13);
		arrayPoint.add(point14);
		arrayPoint.add(point15);

		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(0);
		permutation.add(5);
		permutation.add(12);
		permutation.add(13);
		permutation.add(14);
		permutation.add(15);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroup4, arrayPoint);
		return modelGroup;
	}

	public ModelGroup createModelGroup5() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();

		arrayPoint.add(point9);
		arrayPoint.add(point8);
		arrayPoint.add(point16);
		arrayPoint.add(point17);
		arrayPoint.add(point18);
		arrayPoint.add(point10);

		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(9);
		permutation.add(8);
		permutation.add(16);
		permutation.add(17);
		permutation.add(18);
		permutation.add(10);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroup5, arrayPoint);
		return modelGroup;
	}

	public ModelGroup createModelGroup6() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();

		arrayPoint.add(point11);
		arrayPoint.add(point10);
		arrayPoint.add(point18);
		arrayPoint.add(point19);
		arrayPoint.add(point20);
		arrayPoint.add(point21);

		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(11);
		permutation.add(10);
		permutation.add(18);
		permutation.add(19);
		permutation.add(20);
		permutation.add(21);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroup6, arrayPoint);
		return modelGroup;
	}

	public ModelGroup createModelGroup7() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();

		arrayPoint.add(point13);
		arrayPoint.add(point12);
		arrayPoint.add(point11);
		arrayPoint.add(point21);
		arrayPoint.add(point22);
		arrayPoint.add(point23);

		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(13);
		permutation.add(12);
		permutation.add(11);
		permutation.add(21);
		permutation.add(22);
		permutation.add(23);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroup7, arrayPoint);
		return modelGroup;
	}

	public ModelGroup createModelGroupCenter() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();
		arrayPoint.add(point3);
		arrayPoint.add(point9);
		arrayPoint.add(point5);
		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(3);
		permutation.add(9);
		permutation.add(5);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroupCenter, arrayPoint);
		return modelGroup;
	}

}
