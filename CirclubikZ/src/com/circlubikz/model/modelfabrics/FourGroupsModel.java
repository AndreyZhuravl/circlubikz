package com.circlubikz.model.modelfabrics;

import java.util.ArrayList;

import android.graphics.Color;
import android.graphics.Point;

import com.circlubikz.model.AbsModel;
import com.circlubikz.model.ItemBik;
import com.circlubikz.model.ModelGroup;

public class FourGroupsModel extends AbsModel {
	float radius = 52;
	String color1 = "#123456";
	String color2 = "#654321";
	String color3 = "#ffffff";
	String color4 = "#ffff00";

	Point pointCenterGroup1 = new Point(268, 200);
	Point point0 = new Point(144, 268);
	Point point1 = new Point(144, 128);
	Point point2 = new Point(270, 54);
	Point point3 = new Point(394, 128);
	Point point4 = new Point(394, 268);
	Point point5 = new Point(270, 340);

	Point pointCenterGroup2 = new Point(518, 200);
	Point point6 = new Point(518, 54);
	Point point7 = new Point(640, 128);
	Point point8 = new Point(640, 268);
	Point point9 = new Point(518, 340);

	Point pointCenterGroup3 = new Point(394, 406);
	Point point10 = new Point(518, 480);
	Point point11 = new Point(394, 552);
	Point point12 = new Point(268, 480);

	Point pointCenterGroupCenter = new Point(394, 268);

	public FourGroupsModel() {
		super();
		initGroupsHolder();
	}

	private void initGroupsHolder() {
		mHolder.add(createModelGroupCenter());
		mHolder.add(createModelGroup1());
		mHolder.add(createModelGroup2());
		mHolder.add(createModelGroup3());
	}

	public ModelGroup createModelGroup1() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();

		arrayPoint.add(point0);
		arrayPoint.add(point1);
		arrayPoint.add(point2);
		arrayPoint.add(point3);
		arrayPoint.add(point4);
		arrayPoint.add(point5);

		mItemBikList.add(new ItemBik(point0, radius, Color.parseColor(color1)));
		mItemBikList.add(new ItemBik(point1, radius, Color.parseColor(color1)));
		mItemBikList.add(new ItemBik(point2, radius, Color.parseColor(color1)));
		mItemBikList.add(new ItemBik(point3, radius, Color.parseColor(color2)));
		mItemBikList.add(new ItemBik(point4, radius, Color.parseColor(color2)));
		mItemBikList.add(new ItemBik(point5, radius, Color.parseColor(color2)));

		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(0);
		permutation.add(1);
		permutation.add(2);
		permutation.add(3);
		permutation.add(4);
		permutation.add(5);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroup1, arrayPoint);
		return modelGroup;
	}

	public ModelGroup createModelGroup2() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();

		arrayPoint.add(point4);
		arrayPoint.add(point3);
		arrayPoint.add(point6);
		arrayPoint.add(point7);
		arrayPoint.add(point8);
		arrayPoint.add(point9);

		mItemBikList.add(new ItemBik(point6, radius, Color.parseColor(color3)));
		mItemBikList.add(new ItemBik(point7, radius, Color.parseColor(color3)));
		mItemBikList.add(new ItemBik(point8, radius, Color.parseColor(color3)));
		mItemBikList.add(new ItemBik(point9, radius, Color.parseColor(color2)));
		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(4);
		permutation.add(3);
		permutation.add(6);
		permutation.add(7);
		permutation.add(8);
		permutation.add(9);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroup2, arrayPoint);
		return modelGroup;
	}

	public ModelGroup createModelGroup3() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();

		arrayPoint.add(point5);
		arrayPoint.add(point4);
		arrayPoint.add(point9);
		arrayPoint.add(point10);
		arrayPoint.add(point11);
		arrayPoint.add(point12);
		mItemBikList.add(new ItemBik(point10, radius, Color.parseColor(color4)));
		mItemBikList.add(new ItemBik(point11, radius, Color.parseColor(color4)));
		mItemBikList.add(new ItemBik(point12, radius, Color.parseColor(color4)));
		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(5);
		permutation.add(4);
		permutation.add(9);
		permutation.add(10);
		permutation.add(11);
		permutation.add(12);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroup3, arrayPoint);
		return modelGroup;
	}

	public ModelGroup createModelGroupCenter() {
		ArrayList<Point> arrayPoint = new ArrayList<Point>();
		arrayPoint.add(point3);
		arrayPoint.add(point9);
		arrayPoint.add(point5);
		ModelGroup modelGroup;
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		permutation.add(3);
		permutation.add(9);
		permutation.add(5);
		modelGroup = new ModelGroup(this, permutation, pointCenterGroupCenter, arrayPoint);
		return modelGroup;
	}

}
