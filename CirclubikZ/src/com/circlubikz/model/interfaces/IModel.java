package com.circlubikz.model.interfaces;

import java.util.ArrayList;

import com.circlubikz.model.ItemBik;

public interface IModel {

	ArrayList<ItemBik> getListBiks();

	ItemBik getItem(int i);

	void replaceItem(int i, ItemBik item);

}
