package com.circlubikz.model.interfaces;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;

public interface IViewModel {

	public abstract void draw(Canvas canvas);

	public abstract void shiftGroup(MotionEvent event, Context context);

	public abstract void fixedGroup(MotionEvent event, Context context);
	
	public abstract void catchGroup(MotionEvent event, Context context);

	public abstract void rotateGroup(int groupNumber, int shiftPositionNumber);

	public abstract void setViewListener(View mainView);

	public abstract void returnHistory();

	public abstract void returnHistory1Step();

	public abstract void startStopMoving();

	public abstract String getStepNumber();

}