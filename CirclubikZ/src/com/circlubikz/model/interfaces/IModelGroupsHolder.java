package com.circlubikz.model.interfaces;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;

public interface IModelGroupsHolder {

	void draw(Canvas canvas);

	boolean onTouch(MotionEvent event, Context mContext);

	void rotateGroup(int groupNumber, int shiftPositionNumber);

	void setViewListener(View mainView);

	void returnHistory();
	
	public void updateModel();

	void returnHistory1Step();

	void startStopMoving();

	String getStepNumber();

}
