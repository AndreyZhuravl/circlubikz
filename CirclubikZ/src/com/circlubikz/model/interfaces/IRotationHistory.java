package com.circlubikz.model.interfaces;

import java.util.Map;

public interface IRotationHistory {
	public void addRotation(int groupNumber, int shift);
	public void clearHistory();
	public int getHistorySize();
	public Map<String, Integer> getRotationByNumber(int number);
	public Map<String, Integer> removeLast();
	public Map<String, Integer>getLast();
}
