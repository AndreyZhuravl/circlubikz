package com.circlubikz.model;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;

import com.circlubikz.model.interfaces.IModelGroupsHolder;
import com.circlubikz.model.interfaces.IViewModel;
import com.circlubikz.model.modelfabrics.ElevenGroupsModel;

public class ModelGroupsHolder implements IModelGroupsHolder {

	IViewModel mModel;
	Context mContext;

	public ModelGroupsHolder(Context context) {
		mContext = context;
		mModel = new ElevenGroupsModel(context);
	}
	
	@Override
	public void updateModel(){
		mModel = new ElevenGroupsModel(mContext);
	}

	@Override
	public void draw(Canvas canvas) {
		mModel.draw(canvas);
	}

	@Override
	public boolean onTouch(MotionEvent event, Context context) {
		boolean result = true;
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			mModel.catchGroup(event, context);
		}
		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			mModel.shiftGroup(event, context);
		}
		if (event.getAction() == 1) {
			mModel.fixedGroup(event, context);
			result = false;
		}
		return result;
	}

	@Override
	public void rotateGroup(int groupNumber, int shiftPositionNumber) {
		mModel.rotateGroup(groupNumber, shiftPositionNumber);		
	}

	@Override
	public void setViewListener(View mainView) {
		mModel.setViewListener(mainView);
		
	}

	@Override
	public void returnHistory() {
		mModel.returnHistory() ;
		
	}

	@Override
	public void returnHistory1Step() {
		mModel.returnHistory1Step();
		
	}

	@Override
	public void startStopMoving() {
		mModel.startStopMoving();		
	}

	@Override
	public String getStepNumber() {
		// TODO Auto-generated method stub
		return mModel.getStepNumber();
	}

}
