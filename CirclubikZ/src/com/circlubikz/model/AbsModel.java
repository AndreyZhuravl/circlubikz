package com.circlubikz.model;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;

import com.circlubikz.model.interfaces.IModel;
import com.circlubikz.model.interfaces.IOnGroupStopedListener;
import com.circlubikz.model.interfaces.IRotationHistory;
import com.circlubikz.model.interfaces.IViewModel;

public class AbsModel implements IModel, IViewModel {

	protected ArrayList<ItemBik> mItemBikList;
	protected ArrayList<ModelGroup> mHolder;
	private View mMainView;
	IRotationHistory mRotationHistory;
	private boolean mMovingStop;
	private boolean mMovingStart;

	public AbsModel() {
		mItemBikList = new ArrayList<ItemBik>();
		mHolder = new ArrayList<ModelGroup>();
		mRotationHistory = new RotationHistory();
		mMovingStop = true;
		mMovingStart = false;
	}

	@Override
	public ArrayList<ItemBik> getListBiks() {
		return mItemBikList;
	}

	@Override
	public ItemBik getItem(int i) {
		return mItemBikList.get(i);
	}

	@Override
	public void draw(Canvas canvas) {
		// for (ModelGroup group : mHolder) {
		// group.preDrawQuveringGroup();
		// }
		for (int i = 0; i < mItemBikList.size(); i++) {
			mItemBikList.get(i).draw(canvas);
		}

	}

	@Override
	public void replaceItem(int i, ItemBik item) {
		mItemBikList.remove(i);
		mItemBikList.add(i, item);
	}

	@Override
	public void catchGroup(MotionEvent event, Context context) {
		for (ModelGroup group : mHolder) {
			if (group.isCatched(event)) {
				Vibrator vibro = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
				vibro.vibrate(Constants.TIME_VIBRATE);
				break;
			}
		}
	}

	@Override
	public void shiftGroup(MotionEvent event, Context context) {
		for (ModelGroup group : mHolder) {
			group.shift(event);
		}
	}

	@Override
	public void fixedGroup(MotionEvent event, Context context) {
		for (int i = 0; i < mHolder.size(); i++) {
			mHolder.get(i).setFixedPosition(event, mRotationHistory, i);
		}
		Vibrator vibro = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		vibro.vibrate(Constants.TIME_VIBRATE);
	}

	@Override
	public void rotateGroup(int groupNumber, int shiftPositionNumber) {
		mHolder.get(groupNumber).rotateGroup(shiftPositionNumber, 900, mMainView);
	}

	@Override
	public void setViewListener(View mainView) {
		mMainView = mainView;

	}

	@Override
	public void returnHistory() {
		int size = mRotationHistory.getHistorySize();
		Executor executor = Executors.newFixedThreadPool(1);
		for (int i = 0; i < size; i++) {
			Map<String, Integer> rotation = mRotationHistory.getRotationByNumber(size - i - 1);
			Runnable rotationRunable = mHolder.get(rotation.get(RotationHistory.GROUP_NUMBER)).rotateGroupRunable(
					-rotation.get(RotationHistory.GROUP_SHIFT), 180, mMainView, null);
			executor.execute(rotationRunable);
		}
		mRotationHistory.clearHistory();

	}

	@Override
	public void returnHistory1Step() {
		int size = mRotationHistory.getHistorySize();
		size = size > 0 ? 1 : 0;
		Executor executor = Executors.newFixedThreadPool(1);
		for (int i = 0; i < size; i++) {
			Map<String, Integer> rotation = mRotationHistory.removeLast();
			Runnable rotationRunable = mHolder.get(rotation.get(RotationHistory.GROUP_NUMBER)).rotateGroupRunable(
					-rotation.get(RotationHistory.GROUP_SHIFT), 180, mMainView, null);
			executor.execute(rotationRunable);
		}

	}

	@Override
	public void startStopMoving() {

		if (mMovingStop) {
			mMovingStop = false;
			mMovingStart = true;
			nextRandomStep();
		} else {
			mMovingStop = true;
			mMovingStart = false;
		}

	}

	private void nextRandomStep() {
		if (mMovingStart) {
			Executor executor = Executors.newFixedThreadPool(1);
			Random rand = new Random();
			int lastGroup = 0;
			if (mRotationHistory.getLast() != null) {
				lastGroup = mRotationHistory.getLast().get(RotationHistory.GROUP_NUMBER);
			}
			int groupNumber;
			do {
				groupNumber = rand.nextInt(mHolder.size());
			} while (lastGroup == groupNumber);
			int shift = rand.nextInt(mHolder.get(groupNumber).mPermutation.size() / 2) + 1;
			shift = shift * (rand.nextInt(2) > 0 ? -1 : 1);
			mHolder.get(groupNumber).addNextToHistory(mRotationHistory, groupNumber, shift);
			Runnable rotationRunable = mHolder.get(groupNumber).rotateGroupRunable(shift, 180, mMainView,
					new IOnGroupStopedListener() {
						@Override
						public void onStoped() {
							nextRandomStep();
						}
					});
			executor.execute(rotationRunable);
		}
	}

	@Override
	public String getStepNumber() {
		return "" + mRotationHistory.getHistorySize();
	}

}
