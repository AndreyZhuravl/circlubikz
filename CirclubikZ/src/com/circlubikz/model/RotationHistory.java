package com.circlubikz.model;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import com.circlubikz.model.interfaces.IRotationHistory;

public class RotationHistory implements IRotationHistory {

	public static final String GROUP_SHIFT = "GROUP_SHIFT";
	public static final String GROUP_NUMBER = "GROUP_NUMBER";
	private CopyOnWriteArrayList<Map<String, Integer>> mRotationHistory;

	public RotationHistory() {
		mRotationHistory = new CopyOnWriteArrayList<Map<String, Integer>>();
	}

	@Override
	public void addRotation(int groupNumber, int shift) {
		Map<String, Integer> rotation = new HashMap<String, Integer>();
		rotation.put(GROUP_NUMBER, groupNumber);
		rotation.put(GROUP_SHIFT, shift);
		mRotationHistory.add(rotation);
	}

	@Override
	public void clearHistory() {
		mRotationHistory.clear();
	}

	@Override
	public int getHistorySize() {
		return mRotationHistory.size();
	}

	@Override
	public Map<String, Integer> getRotationByNumber(int number) {
		Map<String, Integer> rotation = null;
		if (mRotationHistory.size() > number) {
			rotation = mRotationHistory.get(number);
		}

		return rotation;
	}

	@Override
	public Map<String, Integer> removeLast() {
		Map<String, Integer> rotation = null;
		if (mRotationHistory.size() > 0) {
			rotation = mRotationHistory.get(mRotationHistory.size() - 1);
			mRotationHistory.remove(mRotationHistory.size() - 1);
		}
		return rotation;
	}

	@Override
	public Map<String, Integer> getLast() {
		Map<String, Integer> rotation = null;
		if (mRotationHistory.size() > 0) {
			rotation = mRotationHistory.get(mRotationHistory.size() - 1);
		}
		return rotation;
	}
}
