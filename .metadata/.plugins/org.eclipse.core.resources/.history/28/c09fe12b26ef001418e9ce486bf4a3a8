package com.circlubikz;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.circlubikz.model.interfaces.IModelGroupsHolder;

public class MainView extends View implements OnTouchListener {

	private IModelGroupsHolder mIModelGroupsHolder;
	Context mContext;
	int mWidth;
	int mHeight;
	int mButtonW = 160;
	int mButtonH = 120;
	RectF leftButtonRect;
	RectF rightButtonRect;
	Paint paintButton;
	boolean isReturn = false;

	public MainView(Context context) {
		super(context);
		mContext = context;
		setOnTouchListener(this);
		paintButton = new Paint();
		paintButton.setColor(Color.GRAY);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int desiredWidth = -2;
		int desiredHeight = -2;

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		// Measure Width
		if (widthMode == MeasureSpec.EXACTLY) {
			// Must be this size
			mWidth = widthSize;
		} else if (widthMode == MeasureSpec.AT_MOST) {
			// Can't be bigger than...
			mWidth = Math.min(desiredWidth, widthSize);
		} else {
			// Be whatever you want
			mWidth = desiredWidth;
		}

		// Measure Height
		if (heightMode == MeasureSpec.EXACTLY) {
			// Must be this size
			mHeight = heightSize;
		} else if (heightMode == MeasureSpec.AT_MOST) {
			// Can't be bigger than...
			mHeight = Math.min(desiredHeight, heightSize);
		} else {
			// Be whatever you want
			mHeight = desiredHeight;
		}
		leftButtonRect = new RectF(mWidth - mButtonW - 20, mHeight - mButtonH - 20, mWidth - 20, mHeight - 20);
		rightButtonRect = new RectF(20, mHeight - mButtonH - 20, mButtonW + 20, mHeight - 20);
		// MUST CALL THIS
		setMeasuredDimension(mWidth, mHeight);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		mIModelGroupsHolder.draw(canvas);		
		canvas.drawRect(leftButtonRect, paintButton);
		canvas.drawRect(rightButtonRect, paintButton);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		boolean result = true;
		RectF rButton = new RectF(20, mHeight - mButtonH - 20, mButtonW + 20, mHeight - 20);
		RectF lButton = new RectF(mWidth - mButtonW - 20, mHeight - mButtonH - 20, mWidth - 20, mHeight - 20);
		if (rButton.contains(event.getX(), event.getY())) {
			mIModelGroupsHolder.updateModel();	
			mIModelGroupsHolder.setViewListener(this);
			isReturn = false;
			result = false;
			invalidate();
		} else if (lButton.contains(event.getX(), event.getY())) {
			if (isReturn != true) {
				mIModelGroupsHolder.returnHistory();
				isReturn = true;
			}
			result = false;
		} else {
			result = mIModelGroupsHolder.onTouch(event, mContext);
			isReturn = false;
			invalidate();
		}
		return result;
	}

	public void setModel(IModelGroupsHolder modelGroupsHolder) {
		mIModelGroupsHolder = modelGroupsHolder;
		mIModelGroupsHolder.setViewListener(this);
	}

}
