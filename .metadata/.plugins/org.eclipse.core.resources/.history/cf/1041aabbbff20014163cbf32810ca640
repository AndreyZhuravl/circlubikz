package com.circlubikz.model;

import java.util.ArrayList;
import java.util.Map;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.circlubikz.model.interfaces.IModel;
import com.circlubikz.model.interfaces.IRotationHistory;

public class ModelGroup {

	IModel mModel;
	ArrayList<Integer> mPermutation;
	Boolean mIsGroupCatched = false;
	float mCatchedX, mCatchedY;
	float mCenterX, mCenterY;
	ArrayList<Point> mFixedCenterPoints;
	double mCurrentQuiveringSize = 1.0;
	private double mCurrentTime;
	Thread mQueringTread;
	private Thread mAnimationRotateThread;
	Rect mGroupRectangle;

	public ModelGroup(IModel model, ArrayList<Integer> permutation, Point center, ArrayList<Point> fixedCenterPoint) {
		mModel = model;
		mPermutation = permutation;
		mCenterX = center.x;
		mCenterY = center.y;
		mFixedCenterPoints = fixedCenterPoint;
		mCurrentTime = 10;

		float top = 10000, left = 10000, right = -1, bottom = -1;
		for (int i = 0; i < mPermutation.size(); i++) {
			ItemBik item = mModel.getItem(mPermutation.get(i));
			if (item.x - item.r < left)
				left = item.x - item.r;
			if (item.x + item.r > right)
				right = item.x + item.r;
			if (item.y - item.r < top)
				top = item.y - item.r;
			if (item.y + item.r > bottom)
				bottom = item.y + item.r;
		}
		float max = Math.max(bottom - top, right - left) / 2 + 5;

		mGroupRectangle = new Rect((int) (mCenterX - max), (int) (mCenterY - max), (int) (mCenterX + max),
				(int) (mCenterY + max));
	}

	public Rect getGroupRectangle() {
		return mGroupRectangle;
	}

	public boolean isCatched(MotionEvent event) {

		boolean result = false;
		mCatchedX = event.getX();
		mCatchedY = event.getY();
		tryGroupAreaIsCatched();
		if (mIsGroupCatched) {
			setGroupItemsCatched();
			startQuivering();
		}
		return mIsGroupCatched;
	}

	private void tryGroupItemsIsCatched() {
		for (int i = 0; i < mPermutation.size(); i++) {
			ItemBik item = mModel.getItem(mPermutation.get(i));
			if (item.isCatched(mCatchedX, mCatchedY)) {
				mIsGroupCatched = true;
			}
		}
	}
	private void tryGroupAreaIsCatched() {
		Point[] points  = new Point[mPermutation.size()];
		Point test = new Point((int)mCatchedX, (int)mCatchedY);
		for (int i = 0; i < mPermutation.size(); i++) {
			ItemBik item = mModel.getItem(mPermutation.get(i));
			points[i] = new Point((int)item.x,(int)item.y);
		}
		if(contains(test, points)){
			mIsGroupCatched = true;
		}
	}
	
	public boolean contains(Point test, Point[] points) {
	      int i;
	      int j;
	      boolean result = false;
	      for (i = 0, j = points.length - 1; i < points.length; j = i++) {
	        if ((points[i].y > test.y) != (points[j].y > test.y) &&
	            (test.x < (points[j].x - points[i].x) * (test.y - points[i].y) / (points[j].y-points[i].y) + points[i].x)) {
	          result = !result;
	         }
	      }
	      return result;
	    }

	private void startQuivering() {
		mQueringTread = new Thread(new Runnable() {
			@Override
			public void run() {

				while (mIsGroupCatched) {
					try {
						Thread.sleep(60);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					preDrawQuveringGroup();
				}

			}
		});
		mQueringTread.start();
	}

	private void setGroupItemsCatched() {
		for (int i = 0; i < mPermutation.size(); i++) {
			ItemBik item = mModel.getItem(mPermutation.get(i));
			item.isCatched = true;
		}
	}

	public void shift(MotionEvent event) {
		if (mIsGroupCatched) {
			float x = event.getX();
			float y = event.getY();
			double angle = angleBetweenTwoPointsWithFixedPoint(x, y, mCatchedX, mCatchedY, mCenterX, mCenterY);
			rotateGroupByAngle(angle);
		}
	}

	private void rotateGroupByAngle(double angle) {
		for (int i = 0; i < mPermutation.size(); i++) {
			ItemBik bik = mModel.getItem(mPermutation.get(i));
			bik.rotate(mCenterX, mCenterY, angle);
		}
	}

	public static double from0to2PIAngle(double point1X, double point1Y, double fixedX, double fixedY) {
		double pX = point1X - fixedX;
		double pY = point1Y - fixedY;
		double angle1 = Math.atan2(pY, pX);
		if (pY <= 0) {
			angle1 += 2 * Math.PI;
		}
		return angle1;
	}

	public static double angleBetweenTwoPointsWithFixedPoint(double point1X, double point1Y, double point2X,
			double point2Y, double fixedX, double fixedY) {

		double angle1 = from0to2PIAngle(point1X, point1Y, fixedX, fixedY);
		double angle2 = from0to2PIAngle(point2X, point2Y, fixedX, fixedY);
		double angle = angle1 - angle2;
		if (angle < 0) {
			angle += 2 * Math.PI;
		}
		return angle;
	}

	private int numberSector(double angle) {

		return (int) ((angle + Math.PI / mPermutation.size()) / (2 * Math.PI / mPermutation.size()));
	}

	public void setFixedPosition(MotionEvent event, IRotationHistory rotationHistory, int groupNumber) {
		if (mIsGroupCatched) {
			float x = event.getX();
			float y = event.getY();
			double angle = angleBetweenTwoPointsWithFixedPoint(x, y, mCatchedX, mCatchedY, mCenterX, mCenterY);
			int numberSectorShift = numberSector(angle);
			setFixedPositionByNumber(numberSectorShift);
			addNextToHistory(rotationHistory, groupNumber, numberSectorShift);

		}
		mIsGroupCatched = false;
	}

	private void addNextToHistory(IRotationHistory rotationHistory, int groupNumber, int numberSectorShift) {
		Log.d("555","groupNumber=" + groupNumber + ", numberSectorShift=" + numberSectorShift);
		if ((numberSectorShift != 0) && (numberSectorShift != mPermutation.size())) {
			int numberSectorShiftModule = numberSectorShift % mPermutation.size();
			if ((numberSectorShiftModule > mPermutation.size() / 2)) {
				numberSectorShiftModule = numberSectorShiftModule - mPermutation.size();
			}
			boolean isNeedSkip = false;
			Map<String, Integer> lastAdded = rotationHistory.getLast();
			Log.d("555","lastAdded=" + lastAdded.get(RotationHistory.GROUP_NUMBER));
			if (lastAdded != null && lastAdded.get(RotationHistory.GROUP_NUMBER) == groupNumber) {

				int lastShfit = lastAdded.get(RotationHistory.GROUP_SHIFT);
				if ((lastShfit + numberSectorShiftModule) == 0) {
					isNeedSkip = true;
				} else {
					lastShfit = (lastShfit + mPermutation.size()) % mPermutation.size();
					numberSectorShiftModule += lastShfit;
					numberSectorShiftModule = numberSectorShiftModule % mPermutation.size();
					if ((numberSectorShiftModule > mPermutation.size() / 2)) {
						numberSectorShiftModule = numberSectorShiftModule - mPermutation.size();
					}
					if (numberSectorShiftModule == 0)
						isNeedSkip = true;
				}
				rotationHistory.removeLast();

			}
			if (!isNeedSkip) {
				rotationHistory.addRotation(groupNumber, numberSectorShiftModule);
			}
		}
	}

	private void setFixedPositionByNumber(int numberSectorShift) {
		mCurrentQuiveringSize = 1.0;
		for (int i = 0; i < mPermutation.size(); i++) {
			ItemBik bik = mModel.getItem(mPermutation.get(i));
			ItemBik.sQuieringRadius = ItemBik.ITEM_RADIUS;
			bik.isCatched = false;
			bik.fixedCoordinate(mCenterX, mCenterY, 2 * Math.PI / mPermutation.size() * numberSectorShift,
					mFixedCenterPoints);
		}
		shiftByPermutation(numberSectorShift);
	}

	private void shiftByPermutation(int steps) {
		for (int s = 0; s < mPermutation.size() - (steps % mPermutation.size()); s++) {
			ItemBik bikTemp = mModel.getItem(mPermutation.get(0));
			for (int i = 0; i < mPermutation.size() - 1; i++) {
				mModel.replaceItem(mPermutation.get(i), mModel.getItem(mPermutation.get(i + 1)));
			}
			mModel.replaceItem(mPermutation.get(mPermutation.size() - 1), bikTemp);
		}
	}

	public void preDrawQuveringGroup() {

		getNextQuiveringSize();

	}

	private void getNextQuiveringSize() {
		ItemBik.sQuieringRadius = (float) (0.09 * Math.cos(mCurrentTime++ / Math.PI) + 0.9) * ItemBik.ITEM_RADIUS;
	}

	Boolean isRrotated = false;

	public void rotateGroup(final int shift, final int timeMillisec, final View mainView) {
		if (!isRrotated)
			synchronized (isRrotated) {
				if (!isRrotated) {
					isRrotated = true;
					mAnimationRotateThread = new Thread(new Runnable() {

						@Override
						public void run() {

							int sleepTime = 10;
							double angle = 0.0;
							double angleStop = (2.0 * Math.PI / (double) mPermutation.size()) * (double) shift;
							double delta = (angleStop / (double) timeMillisec) * (double) sleepTime;
							int countTotal = (int) (((double) timeMillisec) / (double) sleepTime);
							int count = 0;
							while (count < countTotal) {
								rotateGroupByAngle(angle);
								mainView.postInvalidate(mGroupRectangle.left, mGroupRectangle.top,
										mGroupRectangle.right, mGroupRectangle.bottom);

								angle += delta;
								count++;
								try {
									Thread.sleep(sleepTime);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}

							}
							setFixedPositionByNumber(mPermutation.size() + shift);
							mainView.postInvalidate(mGroupRectangle.left, mGroupRectangle.top, mGroupRectangle.right,
									mGroupRectangle.bottom);
							isRrotated = false;
							// ModelGroup.this.rotateGroup(1, 900, mainView);
						}
					});
					mAnimationRotateThread.start();
				}
			}
	}

	public Runnable rotateGroupRunable(final int shift, final int timeMillisec, final View mainView) {

		return new Runnable() {

			@Override
			public void run() {

				int sleepTime = 20;
				double angle = 0.0;
				double angleStop = (2.0 * Math.PI / (double) mPermutation.size()) * (double) shift;
				double delta = (angleStop / (double) (timeMillisec * Math.abs(shift))) * (double) sleepTime;
				int countTotal = (int) (((double) timeMillisec * Math.abs(shift)) / (double) sleepTime);
				int count = 0;
				while (count < countTotal) {
					rotateGroupByAngle(angle);
					 mainView.postInvalidate(mGroupRectangle.left, mGroupRectangle.top, mGroupRectangle.right,
					 mGroupRectangle.bottom);

					angle += delta;
					count++;
					try {
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
				setFixedPositionByNumber(mPermutation.size() + shift);
				Vibrator vibro = (Vibrator) mainView.getContext().getSystemService(Context.VIBRATOR_SERVICE);
				vibro.vibrate(Constants.TIME_VIBRATE);
				mainView.postInvalidate(mGroupRectangle.left, mGroupRectangle.top, mGroupRectangle.right,
						mGroupRectangle.bottom);

				try {
					Thread.sleep(5 * sleepTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};

	}
}
